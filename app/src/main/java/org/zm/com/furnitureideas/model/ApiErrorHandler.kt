package org.zm.com.furnitureideas.model

import android.support.annotation.IntDef
import java.lang.Exception
import java.net.SocketTimeoutException
import java.net.UnknownHostException

object ApiErrorHandler {
    /**
     * for handling network layer exceptions
     */
    fun getErrorData(throwable: Throwable): AppError {
        // for farther use will switch between  throwable and customise the AppError message

        return when (throwable) {
            is SocketTimeoutException -> {
                AppError(AppErrorConstants.NETWORK_ERROR, throwable.localizedMessage)
            }
            is UnknownHostException -> {
                AppError(AppErrorConstants.NETWORK_ERROR, throwable.localizedMessage)
            }
            else -> {
                // default case
                AppError(AppErrorConstants.SYSTEM_ERROR, throwable.localizedMessage)
            }
        }
    }

    /**
     * for handling unsuccessful [not in range of 200 ] api calls
     * 400 for business server error
     * 500 for internal server error
     * for farther use handle different messages with Vs diff http status Code
     */
    fun getErrorData(statusCode: Int): AppError {
        return  AppError(AppErrorConstants.BUSINESS_ERROR, "Server error ")
    }
}


class AppError(@AppErrorConstants.ErrorCategory var errorCategory: Int, var message: String)


class AppErrorConstants {
    companion object {
        const val BUSINESS_ERROR = 0
        const val SYSTEM_ERROR = 1
        const val NETWORK_ERROR = 2
    }

    @IntDef(AppErrorConstants.BUSINESS_ERROR, AppErrorConstants.SYSTEM_ERROR, AppErrorConstants.NETWORK_ERROR)
    @Retention(AnnotationRetention.SOURCE)
    annotation class ErrorCategory

}


