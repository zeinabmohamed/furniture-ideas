package org.zm.com.furnitureideas.model.articles;

import org.zm.com.furnitureideas.model.AppError;

public class DataWrapper<R> {
    public AppError error;
    public R data;
}