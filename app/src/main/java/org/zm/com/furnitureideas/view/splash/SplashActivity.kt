package org.zm.com.furnitureideas.view.splash

import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import org.zm.com.furnitureideas.AppNavigator

class SplashActivity : AppCompatActivity() {

    private val SPLASH_DELAY_TIME_OUT: Long = 2000L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Handler().postDelayed({
            navigateToHomeActivity()
        },SPLASH_DELAY_TIME_OUT)
    }

    private fun navigateToHomeActivity() {

        AppNavigator.navigateToHomeActivity(this)
    }
}
