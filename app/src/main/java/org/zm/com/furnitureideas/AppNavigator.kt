package org.zm.com.furnitureideas

import android.app.Activity
import android.content.Intent
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import org.zm.com.furnitureideas.view.home.HomeActivity

/**
 * center point for all app navigation's between screen
 */

object AppNavigator {

    fun navigateToHomeActivity(activity : Activity){

        startActivity(activity, HomeActivity::class.java)
        activity.finish()
    }

    fun loadFragment(activity: FragmentActivity,
                     fragment: Fragment, containerId: Int, isStacked: Boolean) {

        if (!isStacked) {
            activity.supportFragmentManager
                    .beginTransaction().replace(containerId,
                            fragment).commit()
        } else {
            activity.supportFragmentManager.beginTransaction().replace(containerId,
                    fragment,fragment::class.java.name).addToBackStack(null).commit()
        }
    }
    /**
     * to avoid code duplication
     */
    private fun <TA> startActivity(activity: Activity, targetActivityClass: Class<TA>) {
        activity.startActivity(Intent(activity, targetActivityClass))
    }
}