package org.zm.com.furnitureideas.viewmodel.articles

import android.arch.lifecycle.ViewModel
import org.zm.com.furnitureideas.model.DaggerRepositoryComponent
import org.zm.com.furnitureideas.model.RepositoryComponent


abstract class BaseViewModel : ViewModel() {


    private var viewModelComp: RepositoryComponent = DaggerRepositoryComponent
            .builder()
            .build()

    init {
        injectViewModel(viewModelComp)
    }

    abstract fun injectViewModel(viewModelComp: RepositoryComponent)

}
