package org.zm.com.furnitureideas.model.articles

import org.zm.com.furnitureideas.model.articles.dto.GetArticlesApiResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ArticlesWebservice {

    @GET("/api/v1/articles?appDomain=1&locale=de_DE&")
    fun getArticlesWithLimit(@Query("limit") noOfRequestedArticles: Int): Call<GetArticlesApiResponse>

}
