package org.zm.com.furnitureideas.model.articles.dto


import com.google.gson.annotations.SerializedName

data class GetArticlesApiResponse(@SerializedName("_embedded")
                                  val Embedded: Embedded)


data class Embedded(@SerializedName("articles")
                    val articles: List<ArticleItem>?)

data class ArticleItem(@SerializedName("media")
                        val media: List<MediaItem>?,
                       @SerializedName("title")
                        val title: String = "") {
    var isLike: Boolean? = null
}


data class MediaItem(@SerializedName("mimeType")
                     val mimeType: String = "",
                     @SerializedName("uri")
                     val uri: String = "")







