package org.zm.com.furnitureideas.view.home.articles.fragment


import android.app.ProgressDialog
import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_start.*
import org.zm.com.furnitureideas.R
import org.zm.com.furnitureideas.util.AlertUtil
import org.zm.com.furnitureideas.viewmodel.articles.ArticlesViewModel
import org.zm.com.furnitureideas.AppNavigator
import org.zm.com.furnitureideas.view.home.getHomeContainer


/**
 * A simple [Fragment] subclass.
 * Use the [StartFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class StartFragment : Fragment() ,LifecycleOwner {
    private val TAG  : String = StartFragment::class.java.simpleName
    private lateinit var articlesViewModel: ArticlesViewModel

    private var noOfRequestedArticles: Int = 10

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        articlesViewModel  = ViewModelProviders.of(activity!!).get(ArticlesViewModel::class.java)

    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_start, container, false)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         * @return A new instance of fragment StartFragment.
         */
        @JvmStatic
        fun newInstance() = StartFragment()
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        startButton.setOnClickListener {

           var articlesNoString =   noOfArticlesEditText.text.toString()
            if (!articlesNoString.isNullOrEmpty()) {
                noOfRequestedArticles =  Integer.valueOf(articlesNoString)
            }else{
                activity?.let { it1 -> AlertUtil.showToast(it1, getString(R.string.enter_no_articles)) }
                return@setOnClickListener
            }
            getArticles(noOfRequestedArticles)
        }
    }

    private fun getArticles(noOfRequestedArticles: Int) {

        showLoading()
        articlesViewModel.getArticlesWithLimit(noOfRequestedArticles).observe(this, Observer { articlesDataWrapper ->
            hideLoading()
            // handle success case
            articlesDataWrapper?.data?.let {
                articlesViewModel.setArticlesList(it)
                activity?.let { it1 -> AppNavigator.loadFragment(it1, SelectionFragment.newInstance(), getHomeContainer(),true) }

            }
            // handling error
            articlesDataWrapper?.error?.let {
                activity?.let { it1 -> AlertUtil.showToast(it1, it.message) }
            }

        })
    }

    private fun showLoading() {
        progressBar.visibility = View.VISIBLE
        startButton.isEnabled = false
    }
    private fun hideLoading() {
        progressBar.visibility = View.GONE
        startButton.isEnabled = true
    }

}
