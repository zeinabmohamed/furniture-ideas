package org.zm.com.furnitureideas.view.home.articles.fragment


import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_review.*
import org.zm.com.furnitureideas.R
import org.zm.com.furnitureideas.view.home.articles.adapter.ArticlesAdapter
import org.zm.com.furnitureideas.viewmodel.articles.ArticlesViewModel


/**
 * A simple [Fragment] subclass.
 * Use the [ReviewFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class ReviewFragment : Fragment(), LifecycleOwner {
    private val TAG: String = ReviewFragment::class.java.simpleName
    private lateinit var articlesViewModel: ArticlesViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        articlesViewModel = ViewModelProviders.of(activity!!).get(ArticlesViewModel::class.java)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_review, container, false)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         * @return A new instance of fragment StartFragment.
         */
        @JvmStatic
        fun newInstance() = ReviewFragment()
    }


    private val SPAN_COUNT: Int = 2

    private var showLinearView: Boolean = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        reviewPageTitleTextView.text = getString(R.string.review_articles)
        getArticles()

        articlesRecyclerView?.apply {
            layoutManager = LinearLayoutManager(this@ReviewFragment.context, LinearLayoutManager.VERTICAL, false)
            adapter = ArticlesAdapter(showLinearView)
        }

        swipeGridButton.setOnClickListener {
           swipeToView()
        }
    }

    private fun swipeToView() {
        showLinearView = !showLinearView
        swipeGridButton.isSelected = !showLinearView
        articlesRecyclerView?.apply {
            layoutManager = if (showLinearView) {
                LinearLayoutManager(this@ReviewFragment.context, LinearLayoutManager.VERTICAL, false)

            } else {
                GridLayoutManager(this@ReviewFragment.context, SPAN_COUNT, GridLayoutManager.VERTICAL, false)

            }
            adapter = ArticlesAdapter(showLinearView)

        }
        articlesViewModel.getArticlesLiveData().observe(this@ReviewFragment, Observer { data ->


            (articlesRecyclerView?.adapter as (ArticlesAdapter)).apply {
                data?.let { this.updateData(it) }
            }
        })
    }

    private fun getArticles() {
        articlesViewModel.getArticlesLiveData().observe(this, Observer { data ->

            (articlesRecyclerView?.adapter as (ArticlesAdapter)).apply {
                data?.let { this.updateData(it) }
            }

        })
    }

}
