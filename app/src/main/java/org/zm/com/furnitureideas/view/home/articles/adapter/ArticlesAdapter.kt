package org.zm.com.furnitureideas.view.home.articles.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.row_list_article_item.view.*
import org.zm.com.furnitureideas.R
import org.zm.com.furnitureideas.model.articles.dto.ArticleItem
import org.zm.com.furnitureideas.util.ImageLoaderUtil


class ArticlesAdapter(val showLinearView: Boolean) : RecyclerView.Adapter<ArticlesAdapter.ArticleViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder {
        var view: View?

        view = if (showLinearView) {
            LayoutInflater.from(parent.context).inflate(R.layout.row_list_article_item, parent, false)

        } else {
            LayoutInflater.from(parent.context).inflate(R.layout.row_grid_article_item, parent, false)

        }

        return ArticleViewHolder(view)
    }

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) {

        holder.bind(articlesList[position])
    }

    override fun getItemCount(): Int {
        return articlesList.size
    }

    private val articlesList = arrayListOf<ArticleItem>()

    fun updateData(data: List<ArticleItem>) {

        data.let {
            articlesList.clear()
            articlesList.addAll(it)
        }
        notifyDataSetChanged()
    }


    inner class ArticleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(articleItem: ArticleItem) {
            itemView.articleTitleTextView?.text = articleItem.title
            articleItem.media?.get(0)?.uri?.let { ImageLoaderUtil.loadImage(itemView.articleImageView, it) }
            articleItem.isLike?.let {
                itemView.likeArticleImageView.visibility = if (it) {
                    View.VISIBLE
                } else {
                    View.GONE
                }
            }
        }
    }
}
