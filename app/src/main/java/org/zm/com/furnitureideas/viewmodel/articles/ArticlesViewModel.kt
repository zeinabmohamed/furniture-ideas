package org.zm.com.furnitureideas.viewmodel.articles

import android.arch.lifecycle.MutableLiveData
import org.zm.com.furnitureideas.model.articles.ArticlesRepository
import org.zm.com.furnitureideas.model.RepositoryComponent
import org.zm.com.furnitureideas.model.articles.DataWrapper
import org.zm.com.furnitureideas.model.articles.dto.ArticleItem
import javax.inject.Inject


class ArticlesViewModel : BaseViewModel() {
    @Inject
    lateinit var articlesRepository: ArticlesRepository
    private val articlesListLiveData = MutableLiveData<List<ArticleItem>>()

    override fun injectViewModel(viewModelComp: RepositoryComponent) {
        viewModelComp.inject(this)
    }

    fun getArticlesWithLimit(noOfRequestedArticles: Int): MutableLiveData<DataWrapper<List<ArticleItem>>> {
        return articlesRepository.getArticlesWithLimit(noOfRequestedArticles)
    }

    fun setArticlesList(articlesList: List<ArticleItem>) {
        articlesListLiveData.value = articlesList
    }

    fun getArticlesLiveData() = articlesListLiveData

}
