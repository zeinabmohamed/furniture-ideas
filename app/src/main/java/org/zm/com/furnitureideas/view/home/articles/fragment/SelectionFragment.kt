package org.zm.com.furnitureideas.view.home.articles.fragment


import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_selection.*
import org.zm.com.furnitureideas.AppNavigator
import org.zm.com.furnitureideas.R
import org.zm.com.furnitureideas.view.home.articles.adapter.ArticleViewPagerAdapter
import org.zm.com.furnitureideas.view.home.articles.adapter.ArticlesActionsListener
import org.zm.com.furnitureideas.view.home.articles.adapter.FadeOutTransformation
import org.zm.com.furnitureideas.view.home.getHomeContainer
import org.zm.com.furnitureideas.viewmodel.articles.ArticlesViewModel
import android.util.TypedValue


/**
 * A simple [Fragment] subclass.
 * Use the [SelectionFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class SelectionFragment : Fragment(), LifecycleOwner, ArticlesActionsListener {


    private val TAG: String = SelectionFragment::class.java.simpleName
    private lateinit var articlesViewModel: ArticlesViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        articlesViewModel = ViewModelProviders.of(activity!!).get(ArticlesViewModel::class.java)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_selection, container, false)
    }

    companion object {

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         * @return A new instance of fragment StartFragment.
         */
        @JvmStatic
        fun newInstance() = SelectionFragment()
    }

    private var  totalNoOfArticles: Int = 0

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        articlesViewPager?.apply {
            setPageTransformer(true, FadeOutTransformation())
            adapter = activity?.let {
                ArticleViewPagerAdapter(it, this@SelectionFragment)
            }
        }

        val pageMargin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4f, resources
                .displayMetrics).toInt()
        articlesViewPager.pageMargin = pageMargin
        articlesViewModel.getArticlesLiveData().observe(this, Observer { articles ->

            articlesViewPager.adapter?.let {
                articles?.let { it1 ->
                    totalNoOfArticles = it1.size
                    (it as ArticleViewPagerAdapter).updateArticles(it1)
                }
            }

        })

        articlesViewPager.addOnPageChangeListener(object  : ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(p0: Int) {

            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
            }

            override fun onPageSelected(position: Int) {
                ( articlesViewPager.adapter as ArticleViewPagerAdapter).mCurrentItemPosition = position
            }
        })
        handleReviewArticleButtonVisibility()
        reviewArticlesButton.setOnClickListener {
            AppNavigator.loadFragment(context as FragmentActivity, ReviewFragment.newInstance(), getHomeContainer(),true)
        }

    }

    private fun goNextArticle() {
        var nextArticlePosition = articlesViewPager.currentItem + 1
        if (nextArticlePosition < articlesViewPager.adapter?.count!!)
            articlesViewPager.setCurrentItem(nextArticlePosition, true)
    }

    override fun navigateToNextPage() {
        goNextArticle()
    }

    private var noOfLikedArticles: Int = 0

    private var reviewedArticlesNo: Int= 0

    override fun updateNoOfArticles(isCounterUp: Boolean) {
        if (isCounterUp) {
            noOfLikedArticles +=1
        }else{
            noOfLikedArticles -=1
        }
        likedArticlesCounter.text = getString(R.string.liked_articles_no_out_of_totalno,noOfLikedArticles,totalNoOfArticles)

    }

    private fun handleReviewArticleButtonVisibility() {

        reviewArticlesButton.visibility = if(reviewedArticlesNo > 0 && reviewedArticlesNo == totalNoOfArticles ) View.VISIBLE else View.GONE

    }
    override fun articleReviewed() {
        reviewedArticlesNo = 0
        ( articlesViewPager.adapter as ArticleViewPagerAdapter).articles.forEach {
            it.isLike?.let {
                reviewedArticlesNo+=1
            }
        }
        handleReviewArticleButtonVisibility()
    }

}
