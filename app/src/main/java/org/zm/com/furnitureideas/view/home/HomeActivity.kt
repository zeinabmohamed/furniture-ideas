package org.zm.com.furnitureideas.view.home

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import org.zm.com.furnitureideas.AppNavigator
import org.zm.com.furnitureideas.R
import org.zm.com.furnitureideas.view.home.articles.fragment.StartFragment

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        AppNavigator.loadFragment(this, StartFragment.newInstance(), getHomeContainer(),false)
    }


}

fun getHomeContainer() = R.id.homeContainerFrameLayout
