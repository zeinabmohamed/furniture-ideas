package org.zm.com.furnitureideas.model.articles

import android.arch.lifecycle.MutableLiveData
import org.zm.com.furnitureideas.model.ApiConnectionUtil
import org.zm.com.furnitureideas.model.ApiErrorHandler
import org.zm.com.furnitureideas.model.RepoScope
import org.zm.com.furnitureideas.model.articles.dto.ArticleItem
import org.zm.com.furnitureideas.model.articles.dto.GetArticlesApiResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Scope

/**
 * Fetch articles for presentation layer
 *
 * for farther use will have BaseRepository to handle all common points {error handling as EX}
 */

@RepoScope
class ArticlesRepository @Inject constructor() {

    private val articlesWebservice: ArticlesWebservice = ApiConnectionUtil.retrofit().create(ArticlesWebservice::class.java)
    fun getArticlesWithLimit(noOfRequestedArticles: Int): MutableLiveData<DataWrapper<List<ArticleItem>>> {
        val liveData = MutableLiveData<DataWrapper<List<ArticleItem>>>()
        val dataWrapper = DataWrapper<List<ArticleItem>>()

        articlesWebservice.getArticlesWithLimit(noOfRequestedArticles).enqueue(object : Callback<GetArticlesApiResponse> {
            override fun onResponse(call: Call<GetArticlesApiResponse>, response: Response<GetArticlesApiResponse>) {
                var responseBdy = response.body()
                if (response.isSuccessful && responseBdy != null) {
                    dataWrapper.data = responseBdy.Embedded.articles
                    liveData.value = dataWrapper
                }else{
                    dataWrapper.error = ApiErrorHandler.getErrorData(response.code())
                    liveData.value = dataWrapper
                }

            }
            override fun onFailure(call: Call<GetArticlesApiResponse>, t: Throwable) {
                dataWrapper.error = ApiErrorHandler.getErrorData(t)
                liveData.value = dataWrapper

            }
        })
        return liveData
    }
}

