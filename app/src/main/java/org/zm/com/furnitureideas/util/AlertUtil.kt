package org.zm.com.furnitureideas.util

import android.content.Context
import android.widget.Toast

object AlertUtil {
    fun showToast(context: Context, message: String) {
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show()
    }

}
