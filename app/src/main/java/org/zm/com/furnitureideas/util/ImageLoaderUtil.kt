package org.zm.com.furnitureideas.util

import android.widget.ImageView
import com.squareup.picasso.Picasso

object ImageLoaderUtil {
    fun loadImage(imageView: ImageView, uri: String) {
        Picasso.get().load(uri).into(imageView)
    }

}
