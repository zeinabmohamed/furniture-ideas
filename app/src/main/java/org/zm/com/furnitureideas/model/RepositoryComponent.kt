package org.zm.com.furnitureideas.model

import dagger.Component
import org.zm.com.furnitureideas.viewmodel.articles.ArticlesViewModel
import javax.inject.Scope

@Component()
@RepoScope
interface RepositoryComponent {
    fun inject(viewModel: ArticlesViewModel)
}


/**
 * RepoScope to retain Repos instance for ViewModels injected in that scope
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class RepoScope