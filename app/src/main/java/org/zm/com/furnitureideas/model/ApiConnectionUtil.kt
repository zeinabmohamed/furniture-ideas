package org.zm.com.furnitureideas.model

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


/**
 * Singleton Object for connection Utility
 */
object ApiConnectionUtil {


    private val TAG = ApiConnectionUtil::class.java.name
    private val BASE_URL: String = "https://api-mobile.home24.com"

    private var retrofit: Retrofit

    init {
        retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
    }

    /**
     * @return retrofit object used to create the requests
     */
    fun retrofit(): Retrofit {
        return retrofit
    }


}
