package org.zm.com.furnitureideas.view.home.articles.adapter

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import org.zm.com.furnitureideas.R
import org.zm.com.furnitureideas.model.articles.dto.ArticleItem
import kotlinx.android.synthetic.main.row_article_item.view.*
import org.zm.com.furnitureideas.util.ImageLoaderUtil


class ArticleViewPagerAdapter(private val mContext: Context, private val articlesActionsListener: ArticlesActionsListener) : PagerAdapter() {

    val articles = arrayListOf<ArticleItem>()
    var mCurrentItemPosition: Int = 0
    override fun getCount() = articles.size

    override fun isViewFromObject(view: View, obj: Any): Boolean = view == obj
    private val DONE_PAGE: String = "DONE"

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val inflater = LayoutInflater.from(mContext)
        var itemView: View?
        var article = articles[position]
        if (article.title == DONE_PAGE) {
            itemView = inflater.inflate(R.layout.row_article_done_item,
                    container, false) as ViewGroup
        } else {
            itemView = inflater.inflate(R.layout.row_article_item,
                    container, false) as ViewGroup
            article.media?.get(0)?.uri?.let { ImageLoaderUtil.loadImage(itemView.articleImageView, it) }

            itemView.articleTitleTextView.text = article.title
            if (article.title == itemView.articleTitleTextView.text.toString()) {
                article.isLike?.let {
                    itemView.likeButton.isSelected = it
                    itemView.disLikeButton.isSelected = !it
                }
            }

            itemView.likeButton.setOnClickListener {
                var articleItem =  articles[mCurrentItemPosition]
                if (articleItem.isLike == null) {
                    articlesActionsListener.updateNoOfArticles(true)
                }

                container.getChildAt(mCurrentItemPosition)?.let {
                    ( it as ViewGroup).findViewById<ImageButton>(R.id.likeButton).isSelected = true
                }

                container.getChildAt(mCurrentItemPosition)?.let {
                    ( it as ViewGroup).findViewById<ImageButton>(R.id.disLikeButton).isSelected = false
                }
                articleItem.isLike = true
                articlesActionsListener.navigateToNextPage()
                articlesActionsListener.articleReviewed()

            }

            itemView.disLikeButton.setOnClickListener {
                var articleItem =  articles[mCurrentItemPosition]

                articleItem.isLike?.let {
                    if (it) { // has been liked before
                        articlesActionsListener.updateNoOfArticles(false)
                    }
                }
                container.getChildAt(mCurrentItemPosition)?.let {
                    ( it as ViewGroup).findViewById<ImageButton>(R.id.likeButton).isSelected = false
                }

                container.getChildAt(mCurrentItemPosition)?.let {
                    ( it as ViewGroup).findViewById<ImageButton>(R.id.disLikeButton).isSelected = true
                }
                articleItem.isLike = false

                articlesActionsListener.navigateToNextPage()
                articlesActionsListener.articleReviewed()
            }
        }

        container.addView(itemView)
        return itemView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View?)
    }

    fun updateArticles(articles: List<ArticleItem>) {
        this.articles.clear()
        this.articles.addAll(articles)
        this.articles.add(ArticleItem(null, DONE_PAGE))
        notifyDataSetChanged()
    }


}

interface ArticlesActionsListener {
    fun navigateToNextPage()
    fun updateNoOfArticles(isCounterUp: Boolean)
    fun articleReviewed()
}

