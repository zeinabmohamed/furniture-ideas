

Android Furniture Ideas Mobile App|
===================================

This application sample of Furniture items for basic functionality (view item and rate it liked or not ) .

Programing Language: 
- Android 
- Kotlin

Pre-requisites
--------------

- Android SDK 25
- Android Build Tools v25.0.3
- Android Support Repository
- kotlin plugin

Screenshots
-------------
![splash_screen](screenshots/splash.png)
![start_screen](screenshots/start_screen.png)
![selection_screen1](screenshots/selection_screen_1.png)
![selection_screen2](screenshots/selection_screen_2.png)
![selection_screen3](screenshots/selection_screen_3.png)
![review_screen1](screenshots/review_screen_1.png)
![review_screen2](screenshots/review_screen_2.png)

Getting Started
---------------

This App uses the Gradle build system. To build this project, use the
"gradlew build" command or use "Import Project" in Android Studio.

Dependencies
-------
    "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlin_version"
    'com.android.support:appcompat-v7:28.0.0-rc02'
    'com.android.support.constraint:constraint-layout:1.1.3'
    "android.arch.lifecycle:extensions:1.1.1"
    "android.arch.lifecycle:viewmodel:1.1.1"
    // Add Dagger dependencies
    'com.google.dagger:dagger:2.15'
    'com.google.dagger:dagger-android-processor:2.15'
    'com.google.dagger:dagger-compiler:2.15'
    // Add Retrofit dependencies
    'com.squareup.retrofit2:retrofit:2.4.0'
    'com.google.code.gson:gson:2.8.5'
    'com.squareup.retrofit2:converter-gson:2.4.0'
    // Add Picasso dependency
    'com.squareup.picasso:picasso:2.71828'
   
License
-------

Copyright (c) 2018.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
